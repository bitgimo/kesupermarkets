class CreateSupermarkets < ActiveRecord::Migration[6.0]
  def change
    create_table :supermarkets do |t|
      t.string :name
      t.integer :no_of_items
      t.integer :variation
      t.float :total
     

      t.timestamps
    end
  end
end
