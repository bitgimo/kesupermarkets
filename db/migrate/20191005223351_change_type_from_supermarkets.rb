class ChangeTypeFromSupermarkets < ActiveRecord::Migration[6.0]
  def change
    rename_column :supermarkets, :type, :payment_type
  end
end
