class SupermarketsController < ApplicationController

    def index
        if params[:name]
            @supermarkets = Supermarket.where("name ILIKE ? ", "%#{params[:name]}%")
        else
            @supermarkets = Supermarket.all
        end    
        
    end

    def new
        @supermarket = Supermarket.new
    end

    def create
        @supermarket = Supermarket.new(supermarkets_params)

        if @supermarket.save
            redirect_to @supermarket
        else
            render 'new'
        end
    end

    def show
        @supermarket = Supermarket.find(params[:id])
    end

    def edit
        @supermarket = Supermarket.find(params[:id])
    end

    def destroy
        @supermarket = Supermarket.find(params[:id])
        @supermarket.destroy

        redirect_to supermarkets_path
    end



    def update
        @supermarket = Supermarket.find(params[:id])

        if @supermarket.update (supermarkets_params)
            redirect_to @supermarket
        else
            render 'edit'
        end
    end

    private

    def supermarkets_params
        params.require(:supermarket).permit(:name, :no_of_items, :payment_type, :type)
    end

   
end
